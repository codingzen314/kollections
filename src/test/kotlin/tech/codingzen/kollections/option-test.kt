package tech.codingzen.kollections

import dontdelete.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import dontdelete.Option.Companion.none
import dontdelete.Option.Companion.some
import java.util.*
import kotlin.NoSuchElementException

class OptionTests {
  @Test
  fun fold_isNone() = assertEquals("none", none.fold({ "none" }, { "bad" }))

  @Test
  fun fold_isSome() = assertEquals("abc", some("abc").fold({ "none" }, { it }))

  @Test
  fun bind_isNone() = none.let { opt -> assertEquals(opt, opt.bind { some("nope") }) }

  @Test
  fun bind_isSome_caseA() = assertEquals(some("ABC"), some("abc").bind { some(it.toUpperCase()) })

  @Test
  fun bind_isSome_caseB() = assertEquals(none, some("abc").bind { none })

  @Test
  fun map_isNone() = assertEquals(none, none.map { "nope" })

  @Test
  fun map_isSome_caseA() = assertEquals(some("ABC"), some("abc").map { it.toUpperCase() })

  @Test
  fun filter_isNone() = assertEquals(none, none.filter { true })

  @Test
  fun filter_isSome_satisfied() = assertEquals(some(2), some(2).filter { it == 2 })

  @Test
  fun filter_isSome_notSatisfied() = assertEquals(none, some(2).filter { it == 3 })

  @Test
  fun get_isNone() = assertThrows(NoSuchElementException::class.java) { none.get() }

  @Test
  fun get_isSome() = assertEquals(3, some(3).get())

  @Test
  fun getOrNull_isNone() = assertNull(none.getOrNull())

  @Test
  fun getOrNull_isSome() = assertEquals(3, some(3).getOrNull())

  @Test
  fun orElse_isNone() = assertEquals(42, none.orElse(42))

  @Test
  fun orElse_isSome() = assertEquals(3, some(3).orElse(42))

  @Test
  fun orElseGet_isNone() = assertEquals(42, none.orElseGet { 42 })

  @Test
  fun orElseGet_isSome() = assertEquals(3, some(3).orElseGet { 42 })

  @Test
  fun orElseThrow_isNone() =
    assertThrows(IllegalStateException::class.java) { none.orElseThrow { IllegalStateException() } }

  @Test
  fun orElseThrow_isSome() = assertEquals(3, some(3).orElseThrow { IllegalStateException() })

  @Test
  fun isEmpty_isNone() = assertTrue(none.isEmpty())

  @Test
  fun isEmpty_isSome() = assertFalse(some(3).isEmpty())

  @Test
  fun isNotEmpty_isNone() = assertFalse(none.isNotEmpty())

  @Test
  fun isNotEmpty_isSome() = assertTrue(some(3).isNotEmpty())

  @Test
  fun exists_isNone() = assertFalse(none.exists<Int> { it == 3 })

  @Test
  fun exists_isSome() = assertTrue(some(3).exists { it == 3 })

  @Test
  fun count_isNone() = assertEquals(0, none.count())

  @Test
  fun count_isSome() = assertEquals(1, some("abc").count())

  @Test
  fun forEach_isNone() {
    var ref = 0
    none.forEach { ref = 42 }
    assertEquals(0, ref)
  }

  @Test
  fun forEach_isSome() {
    var ref = 0
    some(32).forEach { ref = it }
    assertEquals(32, ref)
  }

  @Test
  fun asIterable_isNone() {
    var ref = 0
    for (value in none.asIterable()) {
      ref = 42
    }
    assertEquals(0, ref)
  }

  @Test
  fun asIterable_isSome() {
    var ref = 0
    for (value in some(32).asIterable()) {
      ref = value
    }
    assertEquals(32, ref)
  }

  @Test
  fun asSequence_isNone() = assertEquals(0, none.asSequence().count())

  @Test
  fun asSequence_isSome() = assertEquals(1, some(3).asSequence().count())

  @Test
  fun toArray_isNone() = assertTrue(none.toArray<Int>().isEmpty())

  @Test
  fun toArray_isSome() = assertTrue(Arrays.equals(arrayOf(32), some(32).toArray()))

  @Test
  fun toSet_isNone() = assertEquals(setOf<Int>(), none.toSet())

  @Test
  fun toSet_isSome() = assertEquals(setOf(32), some(32).toSet())

  @Test
  fun toList_isNone() = assertEquals(listOf<Int>(), none.toList())

  @Test
  fun toList_isSome() = assertEquals(listOf(32), some(32).toList())
}