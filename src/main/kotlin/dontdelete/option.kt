package dontdelete

import tech.codingzen.kollections.SingleIterator
import tech.codingzen.kollections.emptyIterator

/**
 * Represents no value (None) or some value (Some) as an Algebraic Data Type
 *
 * @param <V> type of value stored in this Option
 */
sealed class Option<out V> {
  companion object {
    /**
     * Represents no value
     */
    val none: Option<Nothing> = None

    /**
     * @param <V> type of value stored in this Option
     * @param value value to put into an Option
     * @return Option containing [value]
     */
    fun <V> some(value: V): Option<V> = Some(value)

    /**
     * @param <V> type of value stored in this Option
     * @param value nullable to put into an Option
     * @return None if [value] is null else Some containing [value]
     */
    fun <V> from(value: V?): Option<V> = value?.let { some(it) } ?: none
  }
}

/**
 * ADT instance of None
 */
internal object None : Option<Nothing>()

/**
 * @param <V> type of value stored in this Option
 * @param value value of this Option
 */
data class Some<out V>(val value: V) : Option<V>()

/**
 * @receiver instance of Option
 * @param <V> type of value stored in this Option
 * @param <R> type of fold result
 * @param noneFn function to invoke if this Option is None
 * @param someFn function to invoke if this Option is Some.  The value passed to this function is the value contained in this Option
 * @return the result of invoking either [noneFn] or [someFn].  [noneFn] is invoked if this Option is None, else [someFn]
 * is invoked with the value stored in this Option
 */
inline fun <V, R> Option<V>.fold(noneFn: () -> R, someFn: (V) -> R): R = when (this) {
  is None -> noneFn()
  is Some -> someFn(value)
}

/**
 * @receiver instance of Option
 * @param <V> type of value stored in this Option
 * @param <T> new type of value stored in the result Option
 * @param fn function that transforms the value held in this Option into a new Option
 * @return None if this Option is None, else the result of invoking [fn] with the value held in this Option
 */
inline fun <V, T> Option<V>.bind(fn: (V) -> Option<T>): Option<T> =
  fold({ @Suppress("UNCHECKED_CAST") (this as Option<T>) }, fn)

/**
 * @receiver instance of Option
 * @param <V> type of value stored in this Option
 * @param <T> new type of value stored in the result Option
 * @param fn function that transforms the value held in this Option into a new Option
 * @return None if this Option is None, else a Some storing the result of invoking [fn] with the value held in this Option
 */
inline fun <V, T> Option<V>.map(fn: (V) -> T): Option<T> = bind {
  Option.some(
    fn(it)
  )
}

/**
 * @receiver instance of Option
 * @param <V> type of value stored in this Option
 * @param pred predicate that tests the value stored in this Option
 * @return None if this Option is None or this Option's value does not satisfy [pred], else this Option
 */
fun <V> Option<V>.filter(pred: (V) -> Boolean): Option<V> = bind { if (pred(it)) this else Option.none }

/**
 * @receiver instance of Option
 * @param <V> type of value stored in this Option
 * @return the value stored in this Option if this Option is Some
 * @throws NoSuchElementException if this Option is a None
 */
fun <V> Option<V>.get(): V = orElseThrow { throw NoSuchElementException("Cannot get value on None") }

/**
 * @receiver instance of Option
 * @param <V> type of value stored in this Option
 * @return null if this Option is None, else the value stored in this Option
 */
fun <V> Option<V>.getOrNull(): V? = orElse(null)

/**
 * @receiver instance of Option
 * @param <V> type of value stored in this Option
 * @param default default value to return if this Option is None
 * @return [default] if this Option is None, else the value stored in this Option
 */
fun <V> Option<V>.orElse(default: V): V = fold({ default }, { it })

/**
 * @receiver instance of Option
 * @param <V> type of value stored in this Option
 * @param fn function that supplies the value to return if this Option is None
 * @return the value of invoking [fn] if this Option is None, else the value stored in this Option
 */
fun <V> Option<V>.orElseGet(fn: () -> V): V = fold(fn, { it })

/**
 * @receiver instance of Option
 * @param <V> type of value stored in this Option
 * @param <E> type of Exception to throw if this Option is None
 * @param fn function that supplies the Exception to throw if this Option is None
 * @return the value stored in this Option if this Option is Some
 * @throws E if this Option is None
 */
fun <V, E : Exception> Option<V>.orElseThrow(fn: () -> E): V = fold({ throw fn() }, { it })

/**
 * @receiver instance of Option
 * @return *true* if this Option is None, else *false*
 */
fun Option<*>.isEmpty(): Boolean = fold({ true }, { false })

/**
 * @receiver instance of Option
 * @return *false* if this Option is None, else *true*
 */
fun Option<*>.isNotEmpty(): Boolean = fold({ false }, { true })

/**
 * @receiver instance of Option
 * @param <V> type of value stored in this Option
 * @param pred predicate that tests the value stored in this Option
 * @return *false* if this Option is None or this Option's value does not satisfy [pred], else *true*
 */
fun <V> Option<V>.exists(pred: (V) -> Boolean): Boolean = fold({ false }, pred)

/**
 * @receiver instance of Option
 * @return *0* if this Option is None, else *1*
 */
fun Option<*>.count(): Int = fold({ 0 }, { 1 })

/**
 * @receiver instance of Option
 * @param <V> type of value stored in this Option
 * @param consumer block of code that is invoked with the value stored in this Option
 * @return Unit.  Note this method only invokes [consumer] if this Option is Some
 */
fun <V> Option<V>.forEach(consumer: (V) -> Unit): Unit = fold({ Unit }, consumer)

/**
 * @receiver instance of Option
 * @param <V> type of value stored in this Option
 * @return empty Iterable if this Option is None, else an Iterable over the value in this Option
 */
fun <V> Option<V>.asIterable(): kotlin.collections.Iterable<V> = fold({ emptyIterator }, {
  SingleIterator(it)
}).let { iterator ->
  object : kotlin.collections.Iterable<V> {
    override fun iterator(): Iterator<V> = iterator
  }
}

/**
 * @receiver instance of Option
 * @param <V> type of value stored in this Option
 * @return empty Sequence if this Option is None, else a Sequence over the value in this Option
 */
fun <V> Option<V>.asSequence(): Sequence<V> = asIterable().asSequence()

/**
 * @receiver instance of Option
 * @param <V> type of value stored in this Option
 * @return empty Array if this Option is None, else an Array storing the value in this Option
 */
inline fun <reified V> Option<V>.toArray(): Array<V> = fold({ emptyArray() }, { arrayOf(it) })

/**
 * @receiver instance of Option
 * @param <V> type of value stored in this Option
 * @return empty Set if this Option is None, else a Set storing the value in this Option
 */
fun <V> Option<V>.toSet(): Set<V> = fold({ emptySet() }, { setOf(it) })

/**
 * @receiver instance of Option
 * @param <V> type of value stored in this Option
 * @return empty List if this Option is None, else a List storing the value in this Option
 */
fun <V> Option<V>.toList(): List<V> = fold({ emptyList() }, { listOf(it) })