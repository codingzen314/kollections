package tech.codingzen.kollections

typealias OptionOf<A> = Hk1<Option.W, A>

sealed class Option<out A> :
  Hk1<Option.W, A>,
  Traversable<A>,
  Zeroable<Option.W, A> {
  // Typeclass, Factory interfaces/implementation(s) on the companion object
  companion object :
    Zeroid<W>,
    BuilderFactory<W> {
    override fun <A> zero(): Hk1<W, A> = None

    override fun <A> makeBuilder(): Builder<W, A> = BuilderImpl()

    fun <A> some(element: A): OptionOf<A> = Some(element)
    fun <A> from(element: A?): OptionOf<A> = if (element == null) None else Some(element)
  }

  // Higher Kind
  object W

  // Traversable
  override fun foreach(body: (A) -> Unit) {
    fold({}, body)
  }

  // Zeroable
  override fun zeroid(): Zeroid<W> = Companion

  // Builder
  class BuilderImpl<A> : Builder<W, A> {
    private var result = zero<A>()

    override fun add(element: A) {
      result = some(element)
    }

    override fun toCollection(): Hk1<W, A> = result
  }
}

object None : Option<Nothing>()
data class Some<out A>(val element: A) : Option<A>()

// Higher Kind
fun <A> OptionOf<A>.fix(): Option<A> = this as Option<A>

// Data Structure
inline fun <A, R> OptionOf<A>.fold(noneFn: () -> R, someFn: (A) -> R): R =
  when (val opt = this.fix()) {
    is None -> noneFn()
    is Some -> someFn(opt.element)
  }

// How Kotlin doesn't/does do implicit parameters that scala does
inline fun <A, B> OptionOf<A>.map(crossinline fn: (A) -> B): OptionOf<B> = fix().map(Option, fn)
inline fun <A> OptionOf<A>.filter(crossinline pred: (A) -> Boolean): OptionOf<A> = fix().filter(Option, pred)

fun main() {
  println(Option.some("foo").map { it.toUpperCase() })
  println(Option.some("bar").filter { "bar" == it })
  println(Option.some("bar").filter { "foo" == it })

}