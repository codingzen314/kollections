package tech.codingzen.kollections

/**
 * @param W witness type
 * @param A 1st element type
 */
interface Hk1<out W, out A>

/**
 * @param W witness type
 * @param A 1st element type
 * @param B 2nd element type
 */
typealias Hk2<W, A, B> = Hk1<Hk1<W, A>, B>

/**
 * @param W witness type
 * @param A 1st element type
 * @param B 2nd element type
 * @param B 3rd element type
 */
typealias Hk3<W, A, B, C> = Hk1<Hk2<W, A, B>, C>