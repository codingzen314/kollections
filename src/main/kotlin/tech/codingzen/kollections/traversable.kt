package tech.codingzen.kollections

interface Foldable<out A> {
  fun <R> foldL(initial: R, fn: (A, R) -> R): R
}

/**
 * @param A element type
 */
interface Traversable<out A> : Foldable<A> {
  fun foreach(body: (A) -> Unit)

  override fun <R> foldL(initial: R, fn: (A, R) -> R): R {
    var result = initial
    foreach { element ->
      result = fn(element, result)
    }
    return result
  }
}

interface Iterable<out A> : Traversable<A>, kotlin.collections.Iterable<A> {
  override fun foreach(body: (A) -> Unit) {
    val iter = iterator()
    while (iter.hasNext()) body(iter.next())
  }
}

inline fun <CW, A, B, COLL> COLL.map(factory: BuilderFactory<CW>, crossinline fn: (A) -> B): Hk1<CW, B>
  where COLL : Hk1<CW, A>,
        COLL : Traversable<A> {
  val builder = foldL(factory.makeBuilder<B>()) { element, bldr ->
    bldr.apply { this.add(fn(element)) }
  }
  return builder.toCollection()
}

// this would be on the companion object
interface Zeroid<out CW> {
  fun <A> zero(): Hk1<CW, A>
}

// the implementation type
interface Zeroable<out CW, out A> {
  fun zeroid(): Zeroid<CW>
}

inline fun <CW, A, COLL> COLL.filter(factory: BuilderFactory<CW>, crossinline pred: (A) -> Boolean): Hk1<CW, A>
  where COLL : Hk1<CW, A>,
        COLL : Traversable<A>,
        COLL : Zeroable<CW, A> {
  val builder = foldL(factory.makeBuilder<A>()) { element, bldr ->
    if (pred(element)) bldr.apply { this.add(element) } else bldr
  }
  return builder.toCollection()
}

interface BuilderFactory<CW> {
  fun <A> makeBuilder(): Builder<CW, A>
}

/**
 * @param CW collection witness type
 * @param A element type
 */
interface Builder<CW, A> {
  fun add(element: A): Unit
  fun toCollection(): Hk1<CW, A>
}