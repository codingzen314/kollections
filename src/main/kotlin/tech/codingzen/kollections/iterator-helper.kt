package tech.codingzen.kollections

val emptyIterator: Iterator<Nothing> = object: Iterator<Nothing> {
  override fun hasNext(): Boolean = false
  override fun next(): Nothing = throw NoSuchElementException("Cannot iterate through empty iterator")
}

class SingleIterator<out V>(val value: V): Iterator<V> {
  private var hasNext = true

  override fun hasNext(): Boolean = hasNext

  override fun next(): V {
    if (!hasNext) throw NoSuchElementException("No more elements")
    hasNext = false
    return value
  }
}