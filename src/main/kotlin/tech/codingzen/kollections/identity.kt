package tech.codingzen.kollections

typealias IdentityOf<A> = Hk1<Identity.W, A>

data class Identity<out A>(val element: A) :
  Hk1<Identity.W, A>,
  Traversable<A> {
  object W

  object Factory: BuilderFactory<W> {
    override fun <A> makeBuilder(): Builder<W, A> = IdentityBuilder()
  }

  override fun foreach(body: (A) -> Unit) {
    body(element)
  }
}

class IdentityBuilder<A> : Builder<Identity.W, A> {
  var element: A? = null
  override fun add(element: A) {
    this.element = element
  }

  override fun toCollection(): Hk1<Identity.W, A> =
    element
      ?.let { Identity(it) }
      ?: throw IllegalStateException("You must add an element before converting to Identity")
}

fun <A> IdentityOf<A>.fix(): Identity<A> = this as Identity<A>

inline fun <A, B> IdentityOf<A>.map(crossinline fn: (A) -> B): IdentityOf<B> = fix().map(Identity.Factory, fn)

fun main() {
  println(Identity("foo"))
  println(Identity("foo").map { it.toUpperCase() })
}