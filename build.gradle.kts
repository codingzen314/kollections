plugins {
    kotlin("jvm") version "1.3.50"
    id("com.vanniktech.maven.publish") version "0.8.0"
}

dependencies {
    compile(kotlin("stdlib"))
    testImplementation("org.junit.jupiter:junit-jupiter:5.4.2")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

repositories {
    jcenter()
}